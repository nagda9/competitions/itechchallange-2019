#include <iostream>
#include <vector>
#include <math.h>

#define MOD 1000000007

using namespace std;

int main() {

    //----------------------------------------------------------------
    std::ios::sync_with_stdio(false); // fast io

    using Person = struct {
        int start;
        int speed;
    };

    int N, L, T;
	cin >> N >> L >> T;
	vector<Person> people(N);

    for (auto& i : people)
        cin >> i.start;
    for (auto& i : people)
        cin >> i.speed;
    //----------------------------------------------------------------
    // Adott:
    // N ember, kezdeti pos, kezdeti seb
    // L hosszu kor
    // T ido

    int result = 0;

    for (int t = 1; t <= T ; t++)
    {
        for (int i = 0; i < N; i++)
        {
            people[i].start = (people[i].start + people[i].speed + L) % L ;
        }

        for (int i = 0; i < L; i++)
        {
            int talalkozas_ossz = 0;
            int talalkozas_rossz = 0;
            vector<int> speeds;
            vector<int> egyutt_mozgok;

            for (int j = 0; j < N; j++)
            {
                if (people[j].start == i)
                {
                    speeds.push_back(people[j].speed);
                }
            }

            for (size_t j = 0; j < speeds.size(); j++)
            {
                int x = 1;
                for (int k = j+1; k < speeds.size(); k++)
                {
                    if (speeds[j] == speeds[k])
                    {
                        x++;
                        speeds.erase(speeds.begin() + k);
                        k--;
                    }
                }
                egyutt_mozgok.push_back(x);
            }

            for (int j = 1; j <= egyutt_mozgok.size(); j++)
            {
                talalkozas_ossz += egyutt_mozgok.size() - j;

                for (int k = 1; k <= egyutt_mozgok[j-1]; k++)
                {
                    talalkozas_rossz += egyutt_mozgok[j-1] - k;
                }
            }
            result += talalkozas_ossz - talalkozas_rossz;
        }
    }



    // TODO calculate


    cout << result % MOD << endl;
    return 0;
}
