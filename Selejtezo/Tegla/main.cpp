#include <bits/stdc++.h>
#include <iostream>
#include <vector>
#include <math.h>

//for test------------------------------------------------------------------------
#include <stdlib.h>
//for test------------------------------------------------------------------------

#define Memory 32

using namespace std;

///Bitset

    //Subtractions
    bitset<Memory> operator -(bitset<Memory> B1, bitset<Memory> B2){
        return bitset<Memory>(B1.to_ullong() - B2.to_ullong());
    }

    void operator -=(bitset<Memory>& B1, bitset<Memory> B2){
        B1 =  bitset<Memory>(B1.to_ullong() - B2.to_ullong());
    }


///Read numOfTowers, towers high and opponent name
void load(string& opponent, vector<bitset<Memory>>& towers){
    int N;
    unsigned long long int brickNumber;
    cin >> N;
    towers.resize(N);
    for(int i = 0; i < N; i++){
        cin >> brickNumber;
        bitset<Memory> B(brickNumber);
        towers[i] = B;
    }
    cin >> opponent;
}

void testLoad(string& opponent, vector<bitset<Memory>>& towers){
    int N;
    unsigned long long int brickNumber;
    N = rand()%3+1;
    cout << N << endl;
    towers.resize(N);
    for(int i = 0; i < N; i++){
        brickNumber = rand() % 10 +1;
        bitset<Memory> B(brickNumber);
        towers[i] = B;
        cout << brickNumber << " ";
    }
    cout << endl;
    opponent = "Bela";
}


///TRUE if enemy start, FALSE if me
bool winningStatus(vector<bitset<Memory>>& towers){
    bitset<Memory> B(0);
    for(unsigned i = 0; i < towers.size(); i++){
        B^=towers[i];
    }
    return B.any();
}

///TRUE if game is end, FALSE else
bool endGame(vector<bitset<Memory>>& towers){
    for(unsigned i = 0; i < towers.size(); i++){
        if(towers[i].any())
            return false;
    }
    return true;
}

///Log the enemy step
void enemyStep(vector<bitset<Memory>>& towers){
    int ID;
    unsigned long long int piece;
    cin >> ID >> piece;
    bitset<Memory> B(piece);
    towers[ID-1] -= B;
}

void myStep(vector<bitset<Memory>>& towers, unsigned ID, unsigned long long int piece){
    cout << ID << " "<< piece << endl;
    towers[ID - 1] -= bitset<Memory>(piece);
}

///That's how our program 'thinking'
void myStrategy(vector<bitset<Memory>>& towers){
    //Loosing strategy:
    if(!winningStatus(towers)){
        for(unsigned i = 0; i < towers.size(); i++){
            if(towers[i].any()){
                myStep(towers,i+1,1);
                return;
            }
        }
        cerr << "Something wrong in myStrategy()\n";
    }
    //Winning strategy:
    bitset<Memory> B(0);
    for(unsigned i = 0; i < towers.size(); i++){
        B^=towers[i];
    }

    ///Index of the first 1 in B
    unsigned long long int index;

    for(int i = B.size()-1; i >= 0; i--){
        if(B.test(i)){
            index = i;
            break;
        }
    }

    for(unsigned i = 0; i < towers.size(); i++){

        //If find the column, that we want to change...
        if(towers[i].test(index)){

            //... search, how many piece take
            bitset<Memory> Brest = towers[i];
            for(unsigned j = 0; j <= index; j++){
                if(B.test(j)){
                    Brest.flip(j);
                }
            }
            myStep(towers,i+1,towers[i].to_ullong()-Brest.to_ullong());
            return;
        }

    }


}


int main(){
    //For test-------------------------------------------------------------------------
        srand(time(NULL));

    string opponent;
    vector<bitset<Memory>> towers;
    load(opponent, towers);
    //For test-------------------------------------------------------------------------
    //                            testLoad(opponent,towers);
    //For test-------------------------------------------------------------------------


    bool enemyStart = !winningStatus(towers);
    cout << (enemyStart ? opponent : "Lali") << endl;

    if(enemyStart)
        enemyStep(towers);

    while(!endGame(towers)){
        myStrategy(towers);

        if(endGame(towers)){
            break;
        }
        enemyStep(towers);
    }

    return 0;
}
