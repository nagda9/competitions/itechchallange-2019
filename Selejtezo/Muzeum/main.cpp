#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

struct Coord
{
    float X, Y;
    bool konkav;
};

int irany(int szam){
    if (szam==0){
        return 0;
    }
    else if(szam>0){
        return 1;
    }
    else
        return -1;
}

float meredekseg(Coord honnan, Coord hova){
    return (hova.Y-honnan.Y)/(hova.X-honnan.X);
}

bool konkav_e(float x_1, float x_2, float y_1, float y_2, float m_1, float m_2){
    if (!(x_1==x_2) || !(y_1==y_2)){
            if(x_1*y_1==0){
                if (x_1==0){
                    if (x_2==(-y_1))
                        return 1;
                    else return 0;
                }
                if (y_1==0){
                    if (y_2==x_1)
                        return 1;
                    else return 0;
                }
            }
            else if(x_1*y_1==1){
                if(((x_2==(-x_1))||(x_2==0))&&((y_2==y_1)||(y_2==0)))
                    return 1;
                else return 0;
            }
            else {
                if(((x_2==x_1)||(x_2==0))&&((y_2==(-y_1))||(y_2==0)))
                    return 1;
                else return 0;
            }
        }
    else{
        if (x_1*y_1==1 && m_2>m_1){
                return 1;
        }
        else if (x_1*y_1==(-1) && m_2<m_1){
                return 1;
        }
        else return 0;
        }
    return 0;
}

void konkav_csucsok(vector<Coord>& poly){
    float x_1 = irany(poly[1].X-poly[0].X);
    float y_1 = irany(poly[1].Y-poly[0].Y);
    float m_1 = meredekseg(poly[0], poly[1]);
    for (unsigned int i=1; i<poly.size()-1; i++){
        float x_2 = irany(poly[i+1].X-poly[i].X);
        float y_2 = irany(poly[i+1].Y-poly[i].Y);
        float m_2 = meredekseg(poly[i], poly[i+1]);
        poly[i].konkav = konkav_e(x_1, x_2, y_1, y_2, m_1, m_2);
        x_1 = x_2;
        y_1 = y_2;
        m_1 = m_2;
    }//az utolso es az elso csucsot kulon meg kell nezni
        float x_2 = irany(poly[0].X-poly[poly.size()-1].X);
        float y_2 = irany(poly[0].Y-poly[poly.size()-1].Y);
        float m_2 = meredekseg(poly[poly.size()-1], poly[0]);
        poly[poly.size()-1].konkav = konkav_e(x_1, x_2, y_1, y_2, m_1, m_2);
        x_1 = x_2;
        y_1 = y_2;
        m_1 = m_2;
        x_2 = irany(poly[1].X-poly[0].X);
        y_2 = irany(poly[1].Y-poly[0].Y);
        m_2 = meredekseg(poly[0], poly[1]);
        poly[0].konkav = konkav_e(x_1, x_2, y_1, y_2, m_1, m_2);
    float min_x = poly[0].X;
    unsigned int hanyadik_a_min = 0;
    for (unsigned int i=1; i<poly.size(); i++){
        if(min_x>poly[i].X){
            min_x = poly[i].X;
            hanyadik_a_min = i;
        }
    }
    if (poly[hanyadik_a_min].konkav){
        for (unsigned int i=0; i<poly.size(); i++){
            poly[i].konkav = !poly[i].konkav;
        }
    }
}

vector<vector<Coord>> lathatok_ (vector<Coord> konkav, vector<Coord> objects, vector<Coord> polygon)
{
    vector<vector<Coord>> lathatok;
    for (size_t i = 0; i < konkav.size(); i++)
    {
        vector<Coord> lathato_targy;
        for (size_t j = 0; j < objects.size(); j++)
        {
            Coord kisebb;
            Coord nagyobb;

            if (konkav[i].X <= objects[j].X)
            {
                kisebb.X = konkav[i].X;
                nagyobb.X = objects[j].X;
            }
            else
            {
                kisebb.X = objects[j].X;
                nagyobb.X = konkav[i].X;
            }

            if (konkav[i].Y <= objects[j].Y)
            {
                kisebb.Y = konkav[i].Y;
                nagyobb.Y = objects[j].Y;
            }
            else
            {
                kisebb.Y = objects[j].Y;
                nagyobb.Y = konkav[i].Y;
            }

            Coord v;
            v.X = objects[j].X - konkav[i].X;
            v.Y = objects[j].Y - konkav[i].Y;

            vector<int> elojel (polygon.size());
            for (int k = 0; k < elojel.size(); k++)
            {
                if (v.Y * polygon[k].X - v.X * polygon[k].Y == v.Y * konkav[i].X - v.X * konkav[i].Y)
                {
                    elojel[k] = 0;
                }
                else if (v.Y * polygon[k].X - v.X * polygon[k].Y > v.Y * konkav[i].X - v.X * konkav[i].Y)
                {
                    elojel[k] = -1;
                }
                else if (v.Y * polygon[k].X - v.X * polygon[k].Y < v.Y * konkav[i].X - v.X * konkav[i].Y)
                {
                    elojel[k] = 1;
                }
            }

            vector<int> valtopont;
            for (int k = 0; k < elojel.size(); k++)
            {
                if (elojel[k] != elojel[(k+1)%elojel.size()])
                {
                    valtopont.push_back(k);
                }
                else
                {
                    valtopont.push_back(-1);
                }
            }

            vector<Coord> metszespont;
            for (size_t k = 0; k < valtopont.size(); k++)
            {
                if (v.X == 0 && v.Y == 0)
                {
                    break;
                }
                else
                {
                    if (valtopont[k] != -1)
                    {
                        Coord w;
                        w.X = polygon[(k+1)%elojel.size()].X - polygon[k].X;
                        w.Y = polygon[(k+1)%elojel.size()].Y - polygon[k].Y;

                        Coord P;

                        if (v.X == 0)
                        {
                            P.X = konkav[i].X;
                            P.Y = (w.Y * P.X - w.Y * polygon[k].X + w.X * polygon[k].Y) / w.X;
                        }
                        else if (v.Y == 0)
                        {
                            P.Y = konkav[i].Y;
                            P.X = (w.Y * polygon[k].X - w.X * polygon[k].Y + w.X * P.Y) / w.Y;
                        }
                        else
                        {
                            P.X = (w.X*v.Y*konkav[i].X - w.X*v.X*konkav[i].Y - v.X*w.Y*polygon[k].X + v.X*w.X*polygon[k].Y)/(v.Y*w.X - v.X*w.Y);
                            P.Y = (w.Y*v.Y*konkav[i].X - w.Y*v.X*konkav[i].Y - v.Y*w.Y*polygon[k].X + v.Y*w.X*polygon[k].Y)/(v.Y*w.X - v.X*w.Y);
                        }

                        metszespont.push_back(P);
                    }
                }
            }

            int sum = 0;
            for (int k = 0; k < metszespont.size(); k++)
            {
                if ((kisebb.X < metszespont[k].X && metszespont[k].X < nagyobb. X) || (kisebb.Y < metszespont[k].Y && metszespont[k].Y < nagyobb.Y))
                {
                    sum += 1;
                }
            }

            if (sum == 0)
            {
                lathato_targy.push_back(objects[j]);
            }
        }
        lathatok.push_back(lathato_targy);
    }
    return lathatok;
}

int main() {
    //-------------------------------------------------------------
    std::ios::sync_with_stdio(false); // fast io

    unsigned N, T, K;

    cin >> N >> T >> K;

    vector<Coord> polygon(N);
    vector<Coord> objects(T);

    for (auto& p : polygon)
        cin >> p.X >> p.Y;

    for (auto& o : objects)
        cin >> o.X >> o.Y;

    vector<Coord> cameras;
    //-------------------------------------------------------------
    // N csucsok szama
    // T targyak szama
    // K kamerak max szama

    konkav_csucsok(polygon);

    vector<Coord> konkav;
    for(int i=0; i<polygon.size(); i++){
        if(polygon[i].konkav){
            konkav.push_back(polygon[i]);
        }
    }

    vector<vector<Coord>> lathatok = lathatok_(konkav, objects, polygon);

    while(objects.size() > 0)
    {
        vector<int> jo;

        for (size_t i = 0; i < lathatok.size(); i++)
        {
            int sum = 0;
            for (size_t j = 0; j < lathatok[i].size(); j++)
            {
                for (size_t k = 0; k < objects.size(); k++)
                {
                    if ((lathatok[i][j].X == objects[k].X) && (lathatok[i][j].Y == objects[k].Y))
                    {
                        sum += 1;
                    }
                }
            }
            jo.push_back(sum);
        }

        int maxi = 0;
        int index;
        for (size_t i = 0; i < jo.size(); i++)
        {
            if (jo[i] > maxi)
            {
                maxi = jo[i];
                index = i;
            }
        }

        cameras.push_back(konkav[index]);

        for (size_t i = 0; i < lathatok[index].size(); i++)
        {
            for (size_t j = 0; j < objects.size(); j++)
            {
                if ((objects[j].X == lathatok[index][i].X) && (objects[j].Y == lathatok[index][i].Y))
                {
                    objects.erase(objects.begin() + j);
                }
            }
        }
    }

    // TODO calculate cameras coordinates

    cout << cameras.size() << endl;
    for (auto& c : cameras)
        cout << c.X << " " << c.Y << endl;
}
