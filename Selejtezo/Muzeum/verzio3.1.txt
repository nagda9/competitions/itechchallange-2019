#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

struct Coord
{
    float X, Y;
};

Coord minker (vector<Coord> polygon, int N)
{
    Coord mini = polygon[0];

    for (int i = 0; i < N; i++)
    {
        if (polygon[i].X < mini.X)
        {
            mini.X = polygon[i].X;
        }
    }

    for (int i = 0; i < N; i++)
    {
        if (polygon[i].Y < mini.Y)
        {
            mini.Y = polygon[i].Y;
        }
    }

    return mini;
}

Coord maxker (vector<Coord> polygon, int N)
{
    Coord maxi = polygon[0];

    for (int i = 0; i < N; i++)
    {
        if (polygon[i].X > maxi.X)
        {
            maxi.X = polygon[i].X;
        }
    }

    for (int i = 0; i < N; i++)
    {
        if (polygon[i].Y > maxi.Y)
        {
            maxi.Y = polygon[i].Y;
        }
    }

    return maxi;
}
vector<vector<Coord>> lathatok_ (vector<Coord>& belsopont, vector<Coord>& objects, vector<Coord> polygon)
{
    vector<vector<Coord>> lathatok;
    for (size_t i = 0; i < belsopont.size(); i++)
    {
        vector<Coord> lathato_targy;
        for (size_t j = 0; j < objects.size(); j++)
        {
            Coord kisebb;
            Coord nagyobb;

            if (belsopont[i].X <= objects[j].X)
            {
                kisebb.X = belsopont[i].X;
                nagyobb.X = objects[j].X;
            }
            else
            {
                kisebb.X = objects[j].X;
                nagyobb.X = belsopont[i].X;
            }

            if (belsopont[i].Y <= objects[j].Y)
            {
                kisebb.Y = belsopont[i].Y;
                nagyobb.Y = objects[j].Y;
            }
            else
            {
                kisebb.Y = objects[j].Y;
                nagyobb.Y = belsopont[i].Y;
            }

            Coord v;
            v.X = objects[j].X - belsopont[i].X;
            v.Y = objects[j].Y - belsopont[i].Y;

            vector<int> elojel (polygon.size());
            for (int k = 0; k < elojel.size(); k++)
            {
                if (v.Y * polygon[k].X - v.X * polygon[k].Y == v.Y * belsopont[i].X - v.X * belsopont[i].Y)
                {
                    elojel[k] = 0;
                }
                else if (v.Y * polygon[k].X - v.X * polygon[k].Y > v.Y * belsopont[i].X - v.X * belsopont[i].Y)
                {
                    elojel[k] = -1;
                }
                else if (v.Y * polygon[k].X - v.X * polygon[k].Y < v.Y * belsopont[i].X - v.X * belsopont[i].Y)
                {
                    elojel[k] = 1;
                }
            }

            vector<int> valtopont;
            for (int k = 0; k < elojel.size(); k++)
            {
                if (elojel[k] != elojel[(k+1)%elojel.size()])
                {
                    valtopont.push_back(k);
                }
                else
                {
                    valtopont.push_back(-1);
                }
            }

            vector<Coord> metszespont;
            for (size_t k = 0; k < valtopont.size(); k++)
            {
                if (v.X == 0 && v.Y == 0)
                {
                    break;
                }
                else
                {
                    if (valtopont[k] != -1)
                    {
                        Coord w;
                        w.X = polygon[(k+1)%elojel.size()].X - polygon[k].X;
                        w.Y = polygon[(k+1)%elojel.size()].Y - polygon[k].Y;

                        Coord P;

                        if (v.X == 0)
                        {
                            P.X = belsopont[i].X;
                            P.Y = (w.Y * P.X - w.Y * polygon[k].X + w.X * polygon[k].Y) / w.X;
                        }
                        else if (v.Y == 0)
                        {
                            P.Y = belsopont[i].Y;
                            P.X = (w.Y * polygon[k].X - w.X * polygon[k].Y + w.X * P.Y) / w.Y;
                        }
                        else
                        {
                            P.X = (w.X*v.Y*belsopont[i].X - w.X*v.X*belsopont[i].Y - v.X*w.Y*polygon[k].X + v.X*w.X*polygon[k].Y)/(v.Y*w.X - v.X*w.Y);
                            P.Y = (w.Y*v.Y*belsopont[i].X - w.Y*v.X*belsopont[i].Y - v.Y*w.Y*polygon[k].X + v.Y*w.X*polygon[k].Y)/(v.Y*w.X - v.X*w.Y);
                        }

                        metszespont.push_back(P);
                    }
                }
            }

            int sum = 0;
            for (int k = 0; k < metszespont.size(); k++)
            {
                if ((kisebb.X < metszespont[k].X && metszespont[k].X < nagyobb. X) || (kisebb.Y < metszespont[k].Y && metszespont[k].Y < nagyobb.Y))
                {
                    sum += 1;
                }
            }

            if (sum == 0)
            {
                lathato_targy.push_back(objects[j]);
            }
        }
        lathatok.push_back(lathato_targy);
    }
    return lathatok;
}

int main() {
    //-------------------------------------------------------------
    std::ios::sync_with_stdio(false); // fast io

    unsigned N, T, K;

    cin >> N >> T >> K;

    vector<Coord> polygon(N);
    vector<Coord> objects(T);

    for (auto& p : polygon)
        cin >> p.X >> p.Y;

    for (auto& o : objects)
        cin >> o.X >> o.Y;

    vector<Coord> cameras;
    //-------------------------------------------------------------
    // N csucsok szama
    // T targyak szama
    // K kamerak max szama

    // pont konkav poligonban vizsgalat

    Coord mini = minker(polygon, N);
    Coord maxi = maxker(polygon, N);

    vector<Coord> belsopont;

    cout << endl;
    for (int j = mini.Y; j <= maxi.Y; j++)
    {
        vector<int> elojel (N);
        for (int i = 0; i < N; i++)
        {
            if (polygon[i].Y > j)
            {
                elojel[i] = 1;
            }
            else if (polygon[i].Y < j)
            {
                elojel[i] = -1;
            }
            else
            {
                elojel[i] = 0;
            }
        }

        vector<int> valtopont;
        for (int i = 0; i < N; i++)
        {
            if (elojel[i] != elojel[(i+1)%N])
            {
                valtopont.push_back(i);
            }
            else
            {
                valtopont.push_back(-1);
            }
        }

        vector<float> metszespont;
        for (size_t i = 0; i < valtopont.size(); i++)
        {
            if (valtopont[i] != -1)
            {
                Coord v;
                v.X = polygon[(i+1)%N].X - polygon[i].X;
                v.Y = polygon[(i+1)%N].Y - polygon[i].Y;

                float P;
                P = (v.Y * polygon[i].X - v.X * polygon[i].Y + v.X * j) / v.Y;

                metszespont.push_back(P);
            }
        }

        for (size_t i = 0; i < metszespont.size(); i++)
        {
            for (int k = mini.X; k <= maxi.X; k++)
            {
                if ((i % 2 == 0) && (metszespont[i] <= k && k <= metszespont[(i)+1]))
                {
                    Coord P;
                    P.X = k;
                    P.Y = j;
                    belsopont.push_back(P);
                }
            }
        }
    }

    vector<vector<Coord>> lathatok = lathatok_(belsopont, objects, polygon);


    while(objects.size() > 0)
    {
        vector<int> jo;

        for (size_t i = 0; i < lathatok.size(); i++)
        {
            int sum = 0;
            for (size_t j = 0; j < lathatok[i].size(); j++)
            {
                for (size_t k = 0; k < objects.size(); k++)
                {
                    if ((lathatok[i][j].X == objects[k].X) && (lathatok[i][j].Y == objects[k].Y))
                    {
                        sum += 1;
                    }
                }
            }
            jo.push_back(sum);
        }

        int maxi = 0;
        int index;
        for (size_t i = 0; i < jo.size(); i++)
        {
            if (jo[i] > maxi)
            {
                maxi = jo[i];
                index = i;
            }
        }

        cameras.push_back(belsopont[index]);

        for (size_t i = 0; i < lathatok[index].size(); i++)
        {
            for (size_t j = 0; j < objects.size(); j++)
            {
                if ((objects[j].X == lathatok[index][i].X) && (objects[j].Y == lathatok[index][i].Y))
                {
                    objects.erase(objects.begin() + j);
                }
            }
        }
    }


    // TODO calculate cameras coordinates

    cout << cameras.size() << endl;
    for (auto& c : cameras)
        cout << c.X << " " << c.Y << endl;
}
