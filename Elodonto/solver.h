#ifndef SOLVER_H_INCLUDED
#define SOLVER_H_INCLUDED

#include <vector>
#include <string>
#include <map>
#include "Base.hpp"
#include "Building.hpp"
#include "Builder.hpp"
#include "Mine.hpp"

using namespace std;

class solver {
    public:
        solver();
        vector<string> process(const vector<string>& infos);
        void loadInfos(const vector<string>& infos, vector<string>* c);//Ez tölti be  aszükséges adatokat
        void clearInfos();
        void outTurn(); //Egy körben lévő bemenetet kiírja a konzolra
        ~solver();

        void Step();
        void outCommands(vector<string> commands);
        void firstCommand(vector<string>& commands);

	private:
	    string message;
	    int level;
	    int gameID, teamID, tick; //A játék azonosítója; A csapat azonosítója; Az adott kör száma
	    bool test; //Test-e vagy sem az adott játék
	    int sizeX, sizeY; //A pálya vízszintes és függőleges hosssza
	    int maxtick; //Ennyi körből áll a játék (max)
	    int maxSteelGraphit, defendPrice; //A bázisról egy kör alatt lopható Graphit alapmennyisége, A bázis őrzésének ára
	    int maxCarryedGraphite; //Az építők által vihető Graphit alapmennyisége
	    int academyStartPrice, academyBuildPrice, academyBonus; //Egy akadémia kezdő költsége; A felépítéshez szükséges Graphit mennyisége; A bónusz amivel a termelés és a lopás mennyiségét növeli
	    int gymStartPrice, gymBuildPrice, gymBonus; //U.a., mint az akadémiánál
	    int trapPrice; //Egy csapda ára

	    int guards;
        bool NeedAcademy;
        bool NeedGym;
        vector<int> PossibleAcademy;
        vector<int> PossibleGym;
        vector<int> BuildField; // épülő épület koordinátái

	    map<int, Base> bases;
	    map<int, vector<Building>> buildings;
	    map<int, map<int,Builder>> builders; //map<teamID, map<builderID, Builder>>
	    vector<Mine> mines;
	    vector<string> lastCommands;

        void complete(); //Ez azért kell, mert egy tickkel el van tolva a lépés, vagyis a kövi tickre kell megmondani, hogy mit akarunk lépni
	    void messageChecker(); //Megnézi az üzenetet és ha baj van, kiírja és leáll a program
	    int remainingTime(); //mennyi idő van hátra a játékból
	    bool canBuildHere(int x, int y); //Megvizsgálja, hogy az adott kordinátára lehet e építeni (nem ütközne-e össze másik épülettel, bányával)
        bool NeedGuard(Mine M);
        int GuardNumber(Mine M);
        int getNearestMineCoord(int& x, int& y); //A koordináták mellett a távolságot is visszadobja (Nem kell kihasználni, de lehet)
        bool NeedBuild();
        bool BuildInProgress();
        vector<int> MineDistances(int x, int y);
        int NearestMineDist(int x, int y);
        int FurthestMineDist(int x, int y);
        int BuildTime(int x, int y, int id);
        bool UnfinishedBuilding();
        void PossibleBuildingCoords();
};

#endif // SOLVER_H_INCLUDED
