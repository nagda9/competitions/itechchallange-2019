#include "solver.h"

#include <iostream>
#include <sstream>
#include <math.h>

solver::solver(){
    lastCommands.resize(0);
}

vector<string> solver::process(const vector<string>& infos) {
	vector<string> commands;
	guards = 0;
	clearInfos();
	loadInfos(infos, &commands);
	complete();

    firstCommand(commands);


//	outTurn();
        Step();
//    if(tick < 10)
//        outCommands(commands);


    lastCommands = commands;

    //cout << remainingTime() << endl;

	return commands;
}

void solver::firstCommand(vector<string>& commands){
    stringstream ss;
    string command;
    ss << gameID << ' ' << teamID << ' ' << tick+1;
    getline(ss,command);
    commands.push_back(command);
}

void solver::complete(){
    for(string s : lastCommands){
        int id;
        stringstream ss;
        ss << s;
        ss >> id;
    if(builders[teamID].find(id) != builders[teamID].end())
        builders[teamID][id].complete(ss);
    }
}

void solver::Step(){
    for(pair<int,Builder> P : builders[teamID]){
        Builder* B = &(P.second);

    ///Menjen haza
        if(B->inHome() && B->getGraphite() == 0 && !B->canIGoMining(remainingTime())) //Ha nem érdemes elindulni (ez azért kell, hogy ne járkáljon feleslegesen, mert csapdába léphet)
            continue;
        if(B->canIGoHome(remainingTime())){//Ez akkor indítja vissza a bázisra, amikor már muszály, mert nincs több ideje
            B->goHome();
            continue;
        }
        int xTry = B->getX();
        int yTry = B->getY();
        Mine M;

    ///Bányászás kezdete
        if(B->inMine(M)){
//            if(B->getGraphite() >= M.getDefendPrice() && NeedGuard(M)){//Ha van elég pénz őrizni és kell is
//                B->GUARD();
//                M.increaseGuards();
//
//                continue;
//            }
            if(B->getGraphite() < maxCarryedGraphite){//Ha nincs tele a zsákod
                if(M.getGraphite1Turn()+B->getGraphite() > maxCarryedGraphite){
                    B->COLLECT(maxCarryedGraphite-(B->getGraphite()));
                    continue;
                }
                else{
                    B->COLLECT();
                    continue;
                }
            }
        }
        if(B->inHome() && B->getGraphite() > 0){
            B->DEPOSIT();
            continue;
        }
        if(B->getGraphite() == 0){
            B->getNearestMineCoord(xTry,yTry);
            B->MOVE(xTry, yTry);
            continue;
        }
        if(B->getGraphite() != 0){
            B->goHome();
            continue;
        }
    }
}

void solver::outCommands(vector<string> commands){
    cout << "---------Commands----------\n";
    for(string s : commands)
        cout << s << endl;
    cout << "-------CommandsEnd--------\n";
}


void solver::loadInfos(const vector<string>& infos, vector<string>* c){
    for(string s: infos){
        stringstream ss;
        string type;
        ss << s;
        ss >> type >> ws;
    ///Játék eleji üzenetek
        if(type == "MESSAGE"){
            getline(ss, message);
            messageChecker();
        }
        else
        if(type == "LEVEL")
            ss >> level;
        if(type == "GAMEID")
            ss >> gameID;
        else
        if(type == "TEST")
            ss >> test;
        else
        if(type == "SIZE")
            ss >> sizeX >> sizeY;
        else
        if(type == "MAXTICK")
            ss >> maxtick;
        else
        if(type == "BASEINFO")
            ss >> maxSteelGraphit >> defendPrice;
        else
        if(type == "BUILDERINFO")
            ss >> maxCarryedGraphite;
        else
        if(type == "ACADEMY")
            ss >> academyStartPrice >> academyBuildPrice >> academyBonus;
        else
        if(type == "GYM")
            ss >> gymStartPrice >> gymBuildPrice >> gymBonus;
        else
        if(type == "TRAP")
            ss >> trapPrice;
        else

    ///Körönkénti üzenetek
        if(type == "REQ")
            ss >> gameID >> teamID >> tick;
        else
        if(type == "BASE"){
            int tid,x,y,g;
            ss >> tid >> x >> y >> g;
            Base B(x,y,g);
            bases[tid] = B;
        }
        else
        if(type == "BUILDING"){
            int t, tid,x,y,r;
            ss >> t >> tid >> x >> y >> r;
            Building B(t,x,y,r);
            buildings[tid].push_back(B);
        }
        else
        if(type == "BUILDER"){
            int tid,id,x,y,g;
            ss >> tid >> id >> x >> y;
            if(teamID == tid){
                ss >> g;
                Builder B(id,x,y,g,c,&mines,&bases, maxCarryedGraphite, teamID);                builders[tid][id] = B;
            }else{
                Builder B(id,x,y);
                builders[tid][id] = B;
            }
        }
        if(type == "MINE"){
            int x1, y1, x2, y2, g1t, dp, g;
            ss >> x1 >> y1 >> x2 >> y2 >> g1t >> dp >> g;
            Mine M(x1, y1, x2, y2, g1t, dp, g);
            mines.push_back(M);
        }

    }

}

void solver::clearInfos(){
    bases.clear();
    buildings.clear();
    builders.clear();
    mines.clear();
}

void solver::messageChecker(){
    if(message != "OK"){
        cerr << message << endl;
        exit(7);
    }
}

void solver::outTurn(){
    cout << "MESSAGE " << message << endl
    << "LEVEL " << level << endl
    << "GAMEID " << gameID << endl
    << "TEST " << test << endl
    << "SIZE " << sizeX << ' ' << sizeY << endl
    << "MAXTICK " << maxtick << endl
    << "BASEINFO " << maxSteelGraphit << ' ' << defendPrice << endl
    << "BUILDERINFO " << maxCarryedGraphite << endl
    << "ACADEMY " << academyStartPrice << ' ' << academyBuildPrice << ' ' << academyBonus << endl
    << "GYM " << gymStartPrice << ' ' << gymBuildPrice << ' ' << gymBonus << endl
    << "TRAP " << trapPrice << endl;
    for(pair<int,Base> P : bases)
        cout << P.second << endl;
    for(pair<int,vector<Building>> P : buildings)
        for(Building B : P.second)
            cout << B << endl;
    for(pair<int,map<int,Builder>> P : builders)
        for(pair<int,Builder> P2 : P.second)
            cout << P2.second << endl;
    for(Mine M : mines)
        cout << M << endl;
    cout << "---------------------------\n";
}

bool solver::canBuildHere(int tx, int ty){//t, mint try
    //Ha a pálya szélén van
    if(tx == 0 || ty == 0 || tx == sizeX-1 || ty == sizeY-1)
        return false;

    //Ha bázissal ütközne
    for(pair<int,Base> P : bases)
        if(abs(P.second.getX()-tx) < 3 && abs(P.second.getY()-ty) < 3)
            return false;

    //Ha épülettel ütközne
    for(pair<int,vector<Building>> P : buildings)
        for(Building B : P.second)
            if(abs(B.getX()-tx) < 3 && abs(B.getY()-ty) < 3)
                return false;

    //Ha bányával ütközne
    for(Mine M : mines)
        if(M.getX1()-3 < tx && tx < M.getX2()+3 && M.getY1()-3 < ty && ty < M.getY2()+3)
            return false;

    return true;
}

// van-e építkezés (nem ugyanaz mint az UnfinishedBuilding(), kell mindkettő! (a NeedBuild()-ben is meg kell hívnom egy ilyen funkciójú fv-t, de ott nem hívhatom meg a BuildInProgress()-t)
bool solver::BuildInProgress()
{
    for(pair<int,vector<Building>> P : buildings)
    {
        for(Building B : P.second)
        {
            if (B.getRG() > 0 && NeedBuild() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}


// megéri-e építkezni
bool solver::NeedBuild()
{
    NeedAcademy = false;
    NeedGym = false;

    //PossibleBuildingCoords();

    vector<int> MineDists = MineDistances(bases[teamID].getX(), bases[teamID].getY());
    int MaxDist = FurthestMineDist(bases[teamID].getX(), bases[teamID].getY());


    int T;
    vector<int> MineDistsCopy;

    if (UnfinishedBuilding() == true)
    {
        T = remainingTime() - BuildTime(BuildField[0], BuildField[1] , 1);
    }
    else
    {
        T = remainingTime() - BuildTime(PossibleAcademy[0], PossibleAcademy[1], 1);
    }

    MineDistsCopy = MineDists;
    int WithoutAcademyIncome = 0;

    while (T > 0 && MineDistsCopy.size() > 0)
    {
        int MinDist = MaxDist;
        int index;
        for (size_t i = 0; i < mines.size(); i++)
        {
            if (MineDistsCopy[i] < MinDist)
            {
                MinDist = MineDistsCopy[i];
                index = i;
            }
        }

        int t;
        t = floor(mines[index].getGraphite() / mines[index].getGraphite1Turn()) * (2*MinDist + 1 + ceil(maxCarryedGraphite / mines[index].getGraphite1Turn()));
        T -= t;

        if (T >= 0)
        {
            WithoutAcademyIncome += mines[index].getGraphite();
        }
        else
        {
            WithoutAcademyIncome += floor((t-T)/(2*MinDist + 1 + ceil(maxCarryedGraphite / mines[index].getGraphite1Turn()))) * maxCarryedGraphite;
        }

        MineDistsCopy.erase(MineDistsCopy.begin()+index);
    }

    if (UnfinishedBuilding() == true)
    {
        T = remainingTime() - BuildTime(BuildField[0], BuildField[1] , 2);
    }
    else
    {
        T = remainingTime() - BuildTime(PossibleGym[0], PossibleGym[1], 2);
    }

    MineDistsCopy = MineDists;
    int WithoutGymIncome = 0;

    while (T > 0 && MineDistsCopy.size() > 0)
    {
        int MinDist = MaxDist;
        int index;
        for (size_t i = 0; i < mines.size(); i++)
        {
            if (MineDistsCopy[i] < MinDist)
            {
                MinDist = MineDistsCopy[i];
                index = i;
            }
        }

        int t;
        t = floor(mines[index].getGraphite() / mines[index].getGraphite1Turn()) * (2*MinDist + 1 + ceil(maxCarryedGraphite / mines[index].getGraphite1Turn()));
        T -= t;

        if (T >= 0)
        {
            WithoutGymIncome += mines[index].getGraphite();
        }
        else
        {
            WithoutGymIncome += floor((t-T)/(2*MinDist + 1 + ceil(maxCarryedGraphite / mines[index].getGraphite1Turn()))) * maxCarryedGraphite;
        }

        MineDistsCopy.erase(MineDistsCopy.begin()+index);
    }

    if (UnfinishedBuilding() == true)
    {
        T = remainingTime() - BuildTime(BuildField[0], BuildField[1], 1);
    }
    else
    {
        T = remainingTime() - BuildTime(PossibleAcademy[0], PossibleAcademy[1], 1);
    }

    MineDistsCopy = MineDists;
    int WithAcademyIncome = 0;

    while (T > 0 && MineDistsCopy.size() > 0)
    {
        int MinDist = MaxDist;
        int index;
        for (size_t i = 0; i < mines.size(); i++)
        {
            if (MineDistsCopy[i] < MinDist)
            {
                MinDist = MineDistsCopy[i];
                index = i;
            }
        }

        int t;
        t = floor(mines[index].getGraphite() / (mines[index].getGraphite1Turn() + academyBonus)) * (2*MinDist + 1 + ceil(maxCarryedGraphite / (mines[index].getGraphite1Turn() + academyBonus)));
        T -= t;

        if (T >= 0)
        {
            WithAcademyIncome += mines[index].getGraphite();
        }
        else
        {
            WithAcademyIncome += floor((t-T)/(2*MinDist + 1 + ceil(maxCarryedGraphite / (mines[index].getGraphite1Turn() + academyBonus)))) * maxCarryedGraphite;
        }

        MineDistsCopy.erase(MineDistsCopy.begin()+index);
    }

    if (UnfinishedBuilding() == true)
    {
        T = remainingTime() - BuildTime(BuildField[0], BuildField[1] , 2);
    }
    else
    {
        T = remainingTime() - BuildTime(PossibleGym[0], PossibleGym[1], 2);
    }

    MineDistsCopy = MineDists;
    int WithGymIncome = 0;

    while (T > 0 && MineDistsCopy.size() > 0)
    {
        int MinDist = MaxDist;
        int index;
        for (size_t i = 0; i < mines.size(); i++)
        {
            if (MineDistsCopy[i] < MinDist)
            {
                MinDist = MineDistsCopy[i];
                index = i;
            }
        }

        int t;
        t = floor(mines[index].getGraphite() / mines[index].getGraphite1Turn()) * (2*MinDist + 1 + ceil((maxCarryedGraphite + gymBonus) / mines[index].getGraphite1Turn()));
        T -= t;

        if (T >= 0)
        {
            WithGymIncome += mines[index].getGraphite();
        }
        else
        {
            WithGymIncome += floor((t-T)/(2*MinDist + 1 + ceil((maxCarryedGraphite + gymBonus) / mines[index].getGraphite1Turn()))) * (maxCarryedGraphite + gymBonus);
        }

        MineDistsCopy.erase(MineDistsCopy.begin()+index);
    }

    if (WithAcademyIncome > WithoutAcademyIncome || WithGymIncome > WithoutGymIncome)
    {
        if (WithAcademyIncome > WithGymIncome)
        {
            NeedAcademy = true;
        }
        else
        {
            NeedGym = true;
        }
    }

    if ((NeedAcademy == true || NeedGym == true))
    {
        return true;
    }
    else
    {
        return false;
    }
}


 //építésre optimális pozíció
void solver::PossibleBuildingCoords(){
    vector<int> PossibleAcademy;
        vector<int> PossibleGym;

}


/*
{
    vector<int> MineDists = MineDistances(bases[teamID].getX(), bases[teamID].getY());
    int MaxDist = FurthestMineDist(bases[teamID].getX(), bases[teamID].getY());
    bool good;

    good = false;
    while (good == false && MineDists.size() > 0)
    {
        for (size_t i = 0; i < mines.size(); i++)
        {
            if (MineDists[i] < MinDist)
            {
                MinDist = MineDists[i];
                index = i;
            }
        }
    if (i == 1)
    {
        if (mines[index].getGraphite() > academyBuildPrice)
        {

        }
    }
    else
    {
        if (mines[index].getGraphite() > gymBuildPrice)
        {

        }
    }

        MineDists.erase(MineDists.begin()+index);
    }

}
*/

// épület felépítésének várható ideje
int solver::BuildTime(int x, int y, int id)
{
    int BuildTime = 0;
    vector<int> MineDists = MineDistances(x, y);
    int MaxDist = FurthestMineDist(x, y);
    int BuildPrice;

    if (id == 1)
    {
        BuildPrice = academyBuildPrice;
    }
    else
    {
        BuildPrice = gymBuildPrice;
    }

    int trips = ceil(BuildPrice/maxCarryedGraphite);
    int S = trips;

    while (S > 0 && MineDists.size() > 0)
    {
        int MinDist = MaxDist;
        int index;
        for (size_t i = 0; i < mines.size(); i++)
        {
            if (MineDists[i] < MinDist)
            {
                MinDist = MineDists[i];
                index = i;
            }
        }

        int s = floor(mines[index].getGraphite() / maxCarryedGraphite);
        S -= s;

        if (S >= 0)
        {
            BuildTime += s * (2*MinDist + 1 + ceil(maxCarryedGraphite / mines[index].getGraphite1Turn()));
        }
        else
        {
            BuildTime += (s-S)* (2*MinDist + 1 + ceil(maxCarryedGraphite / mines[index].getGraphite1Turn()));
        }

        MineDists.erase(MineDists.begin()+index);
    }

    return BuildTime;
}

// ha van befejezetlen épület akkor a koordinátáit elmenti és visszatér igazzal
bool solver::UnfinishedBuilding()
{
    for(pair<int,vector<Building>> P : buildings)
    {
        for(Building B : P.second)
        {
            if (B.getRG() > 0)
            {
                BuildField[0] = B.getX();
                BuildField[1] = B.getY();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}


// egy ponttól az összes bánya távolsága
vector<int> solver::MineDistances(int x, int y)
{
    vector<int> MineDists;
    for (size_t i = 0; i < mines.size(); i++)
    {
        int xStart = x;
        int yStart = y;

        if(x < mines[i].getX1())
            x = mines[i].getX1();
        else if(x > mines[i].getX2())
            x = mines[i].getX2();

        if(y < mines[i].getY1())
            y = mines[i].getY1();
        else if(y > mines[i].getY2())
            y = mines[i].getY2();

        int dist = abs(xStart-x)+abs(yStart-y);
        MineDists.push_back(dist);
    }

    return MineDists;
}


// minimum keresés
int solver::NearestMineDist(int x, int y)
{
    vector<int> MineDists = MineDistances(x, y);
    int MinDist = MineDists[0];

    for (size_t i = 0; i < MineDists.size(); i++)
    {
        if(MinDist > MineDists[i])
        {
            MinDist = MineDists[i];
        }
    }

    return MinDist;
}

// maximum keresés
int solver::FurthestMineDist(int x, int y)
{
    vector<int> MineDists = MineDistances(x, y);
    int MaxDist = MineDists[0];

    for (size_t i = 0; i < MineDists.size(); i++)
    {
        if(MaxDist < MineDists[i])
        {
            MaxDist = MineDists[i];
        }
    }

    return MaxDist;
}

// kell-e még őr
bool solver::NeedGuard(Mine M)
{
    if (guards < GuardNumber(M))
        return true;
    return false;
}


// csapatonként hány őrt érdemes állítani abba a bányába, ahol a builder áll
int solver::GuardNumber(Mine M)
{
    int PossibleOwnGuards = 0;
    vector<int> BuildersInMine;
        int sum = 0;
        for (pair<int,Builder> B : builders[teamID])
        {
            if (B.second.getX() >= M.getX1() && B.second.getX() <= M.getX2() && B.second.getY() >= M.getY1() && B.second.getY() <= M.getY2())
            {
                sum += 1;
<<<<<<< HEAD

                if (B.getTeamID() == teamID)
                {
                    if (B.getGraphite() >= M.getDefendPrice())
                    {
                        PossibleOwnGuards += 1;
                    }
=======
                if (B.second.getGraphite() >= M.getDefendPrice())
                {
                    PossibleOwnGuards += 1;
>>>>>>> f19ae91fc6dbee78d712d78924952460c7e9b0b3
                }
            }
        }
        BuildersInMine.push_back(sum);

    vector<int> NumberOfGuards;
    for (size_t i = 0; i < builders.size(); i++)
    {
        int guards = 0;
        while ((BuildersInMine[i]-guards)*M.getGraphite1Turn() - guards*M.getDefendPrice() > 0)
        {
            guards += 1;
        }
        guards -= 1;
        NumberOfGuards.push_back(guards);
    }

    int MaxEnemyGuards = 0;
    for (size_t i = 0; i < NumberOfGuards.size(); i++)
    {
        if (i != teamID)
        {
            if (MaxEnemyGuards < NumberOfGuards[i])
            {
                MaxEnemyGuards = NumberOfGuards[i];
            }
        }
    }

    if (PossibleOwnGuards > MaxEnemyGuards && PossibleOwnGuards <= NumberOfGuards[teamID])
    {
        return MaxEnemyGuards + 1;
    }
    else if (PossibleOwnGuards == MaxEnemyGuards && PossibleOwnGuards <= NumberOfGuards[teamID])
    {
        return MaxEnemyGuards;
    }
    else
    {
        return -1;
    }
}

int solver::remainingTime(){
    return maxtick - tick;
}

int solver::getNearestMineCoord(int& x, int& y){
    if(mines.size() == 0){
        x = -1;
        y = -1;
        cerr << "No mines left!\n";
        return -1;
    }
    int xTry, yTry;
    int dist = mines[0].getNearestCoord(x,y);
    for(Mine M : mines){
        int dist2 = M.getNearestCoord(xTry,yTry);
        if(dist > dist2){
            dist = dist2;
            x = xTry;
            y = yTry;
        }
    }
    return dist;
}

solver::~solver(){
    cout << "DELETED\n";
}
