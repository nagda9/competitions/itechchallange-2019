#include "Base.hpp"

Base::Base(int x, int y, int g): X(x), Y(y), graphit(g)
{}

Base::Base()
{}

ostream&  Base::out(ostream& out){
    out << "Base: " << X << ' ' << Y << ' ' << graphit;
    return out;
}

ostream& operator << (ostream& out, Base B){
    return B.out(out);
}

int Base::getNearestCoord(int& x, int& y){
    int xStart = x;
    int yStart = Y;
    if(x < X-1)
        x = X-1;
    else if(x > X+1)
        x = X+1;

    if(y < Y-1)
        y = Y-1;
    else if(y > Y+1)
        y = Y+1;
    return abs(xStart-x)+abs(yStart-y);
}

int Base::getX(){
    return X;
}

int Base::getY(){
    return Y;
}
