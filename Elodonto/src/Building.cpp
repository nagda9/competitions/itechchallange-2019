#include "Building.hpp"

Building::Building()
{}

Building::Building(int t, int x, int y, int r): buildingType(t), X(x), Y(y), requiredGraphite(r)
{}

ostream&  Building::out(ostream& out){
    out << "Building: " << buildingType << ' ' << X << ' ' << Y << ' ' << requiredGraphite;
    return out;
}

ostream& operator << (ostream& out, Building B){
    return B.out(out);
}

void Building::getNearestCoord(int& x, int& y){
    if(x < X-1)
        x = X-1;
    else if(x > X+1)
        x = X+1;

    if(y < Y-1)
        y = Y-1;
    else if(y > Y+1)
        y = Y+1;
}

int Building::getX(){
    return X;
}

int Building::getY(){
    return Y;
}

int Building::getRG(){
    return requiredGraphite;
}
