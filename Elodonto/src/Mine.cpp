#include "Mine.hpp"
#include <math.h>


Mine::Mine(){
    ourGuards = 0;
}

Mine::Mine(int x1, int y1, int x2, int y2, int g1t, int dp, int g): X1(x1), Y1(y1), X2(x2), Y2(y2), graphit1Turn(g1t), defendPrice(dp), graphit(g){
    ourGuards = 0;
}

ostream&  Mine::out(ostream& out){
    out << "Mine: " << X1 << ' ' << Y1 << ' ' << X2 << ' ' << Y2 << ' ' << graphit1Turn << ' ' << defendPrice << ' ' << graphit;
    return out;
}

ostream& operator << (ostream& out, Mine M){
    return M.out(out);
}

int Mine::getNearestCoord(int& x, int& y){
    int xStart = x;
    int yStart = y;

    if(x < X1)
        x = X1;
    else if(x > X2)
        x = X2;

    if(y < Y1)
        y = Y1;
    else if(y > Y2)
        y = Y2;
    return abs(xStart-x)+abs(yStart-y);
    }

int Mine::getX1(){
    return X1;
}

int Mine::getX2(){
    return X2;
}

int Mine::getY1(){
    return Y1;
}

int Mine::getY2(){
    return Y2;
}

int Mine::getGraphite1Turn(){
    return graphit1Turn;
}

int Mine::getDefendPrice(){
    return defendPrice;
}

int Mine::getGraphite(){
    return graphit;
}

void Mine::increaseGuards(){
    ourGuards++;
}
