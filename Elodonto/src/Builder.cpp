#include "Builder.hpp"
#include <sstream>

Builder::Builder()
{}

Builder::Builder(int id, int x, int y): ID(id), X(x), Y(y)
{
    graphite = -1;
}

Builder::Builder(int id, int x, int y, int g, vector<string>* c, vector<Mine>* m, map<int, Base>* b, int mcg, int tid)
: ID(id), X(x), Y(y), graphite(g), commands(c), mines(m), bases(b), maxCarryedGraphite(mcg), teamID(tid)
{}

ostream&  Builder::out(ostream& out){
    out << "Builder: " << ID << ' ' << X << ' ' << Y << ' ' << graphite;
    return out;
}

ostream& operator << (ostream& out, Builder B){
    return B.out(out);
}

void Builder::MOVE(char c){
    stringstream ss;
    string command;
    ss << ID << " MOVE " << c;
    getline(ss,command);
    commands->push_back(command);
}

void Builder::MOVE(int x, int y){
    char c = move_to(x,y);
        if(c != '?')
            MOVE(c);
}

void Builder::GUARD(){
    stringstream ss;
    string command;
    ss << ID << " GUARD";
    getline(ss,command);
    commands->push_back(command);
}

void Builder::COLLECT(){
    stringstream ss;
    string command;
    ss << ID << " COLLECT";
    getline(ss,command);
    commands->push_back(command);
}

void Builder::COLLECT(int quantity){
    stringstream ss;
    string command;
    ss << ID << " COLLECT " << quantity;
    getline(ss,command);
    commands->push_back(command);
}

void Builder::BUILD(int buildinID){
    stringstream ss;
    string command;
    ss << ID << " BUILD " << buildinID;
    getline(ss,command);
    commands->push_back(command);
}

void Builder::DEPOSIT(){
    stringstream ss;
    string command;
    ss << ID << " DEPOSIT";
    getline(ss,command);
    commands->push_back(command);
}

void Builder::DEPOSIT(int quantity){
    stringstream ss;
    string command;
    ss << ID << " DEPOSIT " << quantity;
    getline(ss,command);
    commands->push_back(command);
}

void Builder::TRAP(int quantity){
    stringstream ss;
    string command;
    ss << ID << " TRAP " << quantity;
    getline(ss,command);
    commands->push_back(command);
}

char Builder::move_to(int x, int y){
    if(x == X){
        if(y < Y)
            return '^';
        if(y > Y)
            return 'V';
    }
    if(x < X){
        return '<';
    }
    if(x > X){
        return '>';
    }
    return '?';
}

void Builder::goHome(){
    int x = X;
    int y = Y;
    (*bases)[teamID].getNearestCoord(x,y);
    MOVE(x, y);
}

void Builder::complete(stringstream& ss){
    string command;
    ss >> command;
    if(command == "MOVE"){
        char dir;
        ss >> dir;
        if(dir == 'V')
            Y++;
        else
        if(dir == '^')
            Y--;
        else
        if(dir == '>')
            X++;
        else
        if(dir == '<')
            X--;

    }else
    if(command == "DEPOSIT"){
        if(inHome())
            graphite = 0;
    }
    if(command == "COLLECT"){
        Mine M;
        if(inMine(M))
            graphite += min(M.getGraphite1Turn(), maxCarryedGraphite-graphite);
    }
}

int Builder::getX(){
    return X;
}

int Builder::getY(){
    return Y;
}

int Builder::getGraphite(){
    return graphite;
}

int Builder::getTeamID(){
    return teamID;
}

bool Builder::inMine(){
    for(Mine M : (*mines))
        if(M.getX1() <= X && X <= M.getX2() && M.getY1() <= Y && Y <= M.getY2())
            return true;
    return false;
}

bool Builder::inMine(Mine& mineInput){
    for(int i = 0; i < mines->size(); i++)
        if((*mines)[i].getX1() <= X && X <= (*mines)[i].getX2() && (*mines)[i].getY1() <= Y && Y <= (*mines)[i].getY2()){
            mineInput = (*mines)[i];
            return true;
        }
    return false;
}

bool Builder::inEnemyBase(){
    for(pair<int,Base> B : (*bases))
        if( B.first != teamID && abs(B.second.getX()-X) <= 1 && abs(B.second.getY()-Y) <= 1 )
            return true;
    return false;
}

bool Builder::inEnemyBase(Base& baseInput){
    for(pair<int,Base> B : (*bases))
        if( B.first != teamID && abs(B.second.getX()-X) <= 1 && abs(B.second.getY()-Y) <= 1 ){
            baseInput = B.second;
            return true;
        }
    return false;
}

bool Builder::inHome(){
    if( abs((*bases)[teamID].getX()-X) <= 1 && abs((*bases)[teamID].getY()-Y) <= 1)
        return true;
}

bool Builder::canIGoMining(int remainingTime){
    int x = X;
    int y = Y;
    int distance = getNearestMineCoord(x,y);
    if(distance != -1 && 2*distance+2 <= remainingTime)//Maradt még bány és van még idő odaés visszaérni(+felvenni valamit és letenni)
        return true;
    return false;
}

bool Builder::canIGoHome(int remainingTime){
    if(inHome())
        return false;
    if(mines->empty())
        return true;
    int x = X;
    int y = Y;
    int distance = (*bases)[teamID].getNearestCoord(x,y);
    if(distance+2 < remainingTime)
        return false;
    return true;
}

int Builder::getNearestMineCoord(int& x, int& y){
    if(mines->size() == 0){
        cerr << "No mines left!\n";
        return -1;
    }
    int xTry, yTry;
    int xStart = x;
    int yStart = y;
    int dist = (*mines)[0].getNearestCoord(x,y);
    for(Mine M : (*mines)){
            xTry = xStart;
            yTry = yStart;
        int dist2 = M.getNearestCoord(xTry,yTry);
        if(dist > dist2){
            dist = dist2;
            x = xTry;
            y = yTry;
        }
    }
    return dist;
}
