#ifndef MINE_HPP
#define MINE_HPP

#include <iostream>

using namespace std;

class Mine
{
    public:
        Mine();
        Mine(int x1, int y1, int x2, int y2, int g1t, int dp, int g);
        ostream& out(ostream& out);
        int getNearestCoord(int& x, int& y);//Beadjuk a kezdő kordinátákat és megváltoztatjuk a vég kordinátákra (ezért kell a referencia) ÉS megadja a távolságot is!!!
        int getX1();
        int getX2();
        int getY1();
        int getY2();
        int getGraphite1Turn();
        int getDefendPrice();
        int getGraphite();
        int getOurGuards();
        void increaseGuards();

    protected:

    private:
        int X1, Y1, X2, Y2;
        int graphit1Turn; //Egy kör alatt max ennyi grafitot lehet kitermelni
        int defendPrice;
        int graphit; //Maradék grafit a bányában
        int ourGuards;
};

ostream& operator << (ostream& out, Mine M);

#endif // MINE_HPP
