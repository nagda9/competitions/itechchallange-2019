#ifndef BASE_HPP
#define BASE_HPP

#include <iostream>

using namespace std;

class Base
{
    public:
        //Azért nincs ID, mert a mapben van eltárolva
        Base();
        Base(int x, int y, int g);
        ostream& out(ostream& out);
        int getNearestCoord(int& x, int& y);//Beadjuk a kezdő kordinátákat és megváltoztatjuk a vég kordinátákra (ezért kell a referencia)
        int getX();
        int getY();
    protected:

    private:
        int X, Y; //A bázis középpontjának koordinátái (3*3-as a bázis)
        int graphit; //Mennyi grafit van a bázison
};

ostream& operator << (ostream& out, Base B);


#endif // BASE_HPP
