#ifndef BUILDER_HPP
#define BUILDER_HPP

#include <iostream>
#include <vector>
#include <map>
#include "Mine.hpp"
#include "Base.hpp"

using namespace std;

class Builder
{
    public:
        Builder();
        Builder(int id, int x, int y);
        Builder(int id, int x, int y, int g, vector<string>* c, vector<Mine>* m, map<int, Base>* b, int mcg, int tid);
        ostream& out(ostream& out);
        int getX();
        int getY();
        int getGraphite();
        int getNearestMineCoord(int& x, int& y); //A koordináták mellett a távolságot is visszadobja (Nem kell kihasználni, de lehet)

        void complete(stringstream& ss);
        //Ezek a parancsok az adott építőnek a lépését rögzítik a commands vektorban
        void MOVE(char c);
        void MOVE(int x, int y);
        void GUARD();
        void COLLECT();
        void COLLECT(int quantity);
        void BUILD(int buildingID);
        void DEPOSIT();
        void DEPOSIT(int quantity);
        void TRAP(int quantity);

        void goHome(); //Hazairányítja a saját bázisra

        char move_to(int x, int y); //Visszatér azzal a karakterrel, amerre mennie kell, hogy (x,y)-ba jusson

        bool inHome();
        bool inEnemyBase();
        bool inEnemyBase(Base& baseInput);
        bool inMine();
        bool inMine(Mine& mineInput);

        bool canIGoMining(int remainingTime); //Megadja, hogy érdemes e elindulni bányászni
        bool canIGoHome(int remainingTime);

    protected:

    private:
        int ID; //Az építő saját indexe
        int X,Y;
        int graphite; //Ennyi grafit van nála
        vector<string>* commands;
        vector<Mine>* mines;
        map<int, Base>* bases;
        int teamID;
        int maxCarryedGraphite;
};

ostream& operator << (ostream& out, Builder B);

#endif // BUILDER_HPP
