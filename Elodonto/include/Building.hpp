#ifndef BUILDING_HPP
#define BUILDING_HPP

#include <iostream>

using namespace std;

class Building
{
    public:
        Building();
        Building(int t, int x, int y, int r);
        ostream& out(ostream& out);
        void getNearestCoord(int& x, int& y);//Beadjuk a kezdő kordinátákat és megváltoztatjuk a vég kordinátákra (ezért kell a referencia)
        int getX();
        int getY();
        int getRG();
    protected:

    private:
        int buildingType;
        int X,Y;
        int requiredGraphite; //Ennyi grafit szükséges még, hogy kész legyen az épület (0, ha kész)
};

ostream& operator << (ostream& out, Building B);

#endif // BUILDING_HPP
